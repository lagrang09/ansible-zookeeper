# ansible-zookeeper
Ansible script to deploy Zookeeper cluster with dynamic configuration support on Debian 8(or higher)


## Script description
This section describe script execution step-by-step.  

1. Check OS compatibility.
This script support **only Debian 8** or higher with **systemd** support.  
2. Prepare environment to Zookeeper installation:
  * stop existing Zookeeper systemd service(in case when you already use this script to deploy Zookeeper)
  * create dedicated Zookeeper user and group
  * create all required folders(binary, configuration, logs, Java GC logs, Zookeeper data and log directories)
3. Download and install Zookeeper binaries.
4. Configure Zookeeper service:
  * create zoo.cfg with custom configuration values which you set.
  * create `myid` file with server ID
  * create dynamic configuration file
  * create zookeeper-env.sh file with custom Zookeeper server parameters, such as *JVM GC, Memory, JMX* settings or Zookeeper specific parameters such as `-Dzookeeper.serverCnxnFactory`(and other parameters which cannot be passed using *zoo.cfg* and support only JVM parameters).
  * create Zookeeper systemd unit file and start service(if explicitly required through variable `zk_systemd_service_start`)  
5. **_Optional_**: configure and start(as Docker container) [Prometheus](prometheus.io) JMX exporter. More information in section [Prometheus JMX exporter](Prometheus JMX exporter).

*Notes*:  
1. script doesn't install JDK or Docker(in case when you want [Prometheus](prometheus.io) JMX exporter). You **have to** install it before running this script(you can use other our role [Ansible JDK](https://github.com/i-knowledge/ansible-java))  
2. most defaults in script assume that you use JDK 8. If you want to run Zookeeper on older versions you have to change many default JVM parameters.

## Configurable variables
Mostly variables contained in this section have reasonable default values and you don't wish to change it.

#### Environment variables
This section contains variables which controls which Zookeeper version will be installed, where it will be installed and so on.

| Variable name | Default Value | Description|
| :--------: | :--------: | :--------: |
| tmp_dir  | /tmp/zookeeper-deploy-{{ zk_cfg_client_port }}   | Temp folder to download Zookeeper binary package, uncompress it, etc.|
| zk_version | 3.5.2-alpha | Zookeeper version deploy to |
| zk_user | zookeeper | Zookeeper service user |
| zk_group | zookeeper | Zookeeper service group |
| zk_bin_dir | /usr/lib/zookeeper-{{ zk_cfg_client_port }} | Folder to install Zookeeper binaries |
| zk_conf_dir | /etc/zookeeper-{{ zk_cfg_client_port }} | Folder with Zookeeper configuration files(zoo.cfg, zoo.cfg.dynamic, etc) |
| zk_conf_link_dir | {{ zk_bin_dir }}/conf | Soft link to `zk_conf_dir` folder. By default create soft link in binary folder to ease access to config folder. |
| zk_cfg_data_dir | /var/lib/zookeeper-{{ zk_cfg_client_port }} | Zookeeper data dir. |
| cfg_data_log_dir | {{ zk_cfg_data_dir }}/txlog | Zookeeper transaction log dir. |
| zk_log_dir | /var/log/zookeeper-{{ zk_cfg_client_port }} | Zookeeper service log folder |
| zk_gc_log_dir | {{ zk_log_dir }}/gc | Zookeeper service JVM GC log folder |
| zk_systemd_service_desc | zookeeper-{{ zk_cfg_client_port }} | Systemd service description(used in systemd unit file)|
| zk_systemd_service_name | zookeeper-{{ zk_cfg_client_port }} | Zookeeper systemd service name |
| zk_systemd_restart_type | always | Possible values: on-success, on-failure, on-abnormal, on-watchdog, on-abort, or always. See more [systemd.service](https://www.freedesktop.org/software/systemd/man/systemd.service.html#Restart=) man page. |
| zk_systemd_pid_file | /var/run/zookeeper-{{ zk_cfg_client_port }}/zk.pid | Zookeeper service PID file location |
| zk_systemd_service_start | True | Indicate whether script have to start Zookeeper service after installation. Set to `False` if you want only install Zookeeper and not start it. |

#### Logging(Zookeeper log4j settings)

| Variable name | Default Value | Description|
| :--------: | :--------: | :--------: |
| zk_server_logger_console_log_threshold | INFO | Logging level for console appender |
| zk_server_logger_threshold | INFO | Logging level for file logger |
| zk_server_logger_maxfilesize | 256MB | Log file max size(if exceeded it will be rolled) |
| zk_server_logger_maxbackupindex | 20 | Max count of rolled files which have to be retained |

#### JMX(Java monitoring)

| Variable name | Default Value | Description|
| :--------: | :--------: | :--------: |
| zk_server_jvm_jmx_enabled | True | Enable JMXby default, local-only) |
| zk_server_jvm_jmx_remote_enabled | False | Enable remote JMX(work only with `zk_server_jvm_jmx_enabled`=`True`) |
| zk_server_jvm_jmx_port | 5555 | JMX port which will accept connections |
| zk_server_jmx_rmi_server_hostname | {{ ansible_host }} | Set RMI server hostname. Useful in cases when Zookeeper server behind some NAT(such as OpenStack VM which typically has public and private IP) and cause errors like [this](http://stackoverflow.com/questions/15685686/java-rmi-connectexception-connection-refused-to-host-127-0-1-1). In this case set RMI hostname to public IP/hostname same as you use to connect to JMX. |
| zk_server_jvm_jmx_auth | "false" | Enable authentication for JMX connections. Disabled by default. Set to "true" to enable |
| zk_server_jvm_jmx_ssl | "false" | Enable SSL for JMX connections. Disabled by default. Set to "true" to enable |

#### Zookeeper JVM settings

| Variable name | Default Value | Description|
| :--------: | :--------: | :--------: |
| zk_server_jvm_params | -Djava.net.preferIPv4Stack=true | Custom JVM parameters |
| zk_server_jvm_jit_flags | -XX:+TieredCompilation | JVM JIT compilation parameters |
| zk_server_jvm_gc_diag_flags | -XX:+PrintSafepointStatistics -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCApplicationConcurrentTime -XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintTenuringDistribution -XX:+PrintAdaptiveSizePolicy -Xloggc:{{ zk_gc_log_dir }}/gc.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=250 -XX:GCLogFileSize=10240K -XX:G1LogLevel=finest | Diagnostic JVM GC flags|
| zk_server_jvm_gc_flags | {{ zk_server_jvm_gc_diag_flags }} -XX:+ClassUnloading -XX:+ClassUnloadingWithConcurrentMark -XX:+UseG1GC -XX:ConcGCThreads={{ ansible_processor_vcpus // 2 }} -XX:ParallelGCThreads={{ ansible_processor_vcpus }} -XX:+ParallelRefProcEnabled | JVM GC flags |
| zk_server_jvm_perf_flags | -XX:+PerfDataSaveToFile -XX:-PerfDisableSharedMem -XX:+AggressiveOpts -XX:+DoEscapeAnalysis -XX:+UseBiasedLocking -XX:+UseFastAccessorMethods -XX:+UseCodeCacheFlushing -XX:+OptimizeStringConcat -XX:+UseCompressedOops -XX:+AlwaysPreTouch | JVM performance/optimizations flags |
| zk_server_jvm_diag_flags | -XX:+UnlockExperimentalVMOptions -XX:+UnlockDiagnosticVMOptions -XX:+PrintCommandLineFlags -XX:+PrintConcurrentLocks -XX:ErrorFile={{ zk_log_dir }}/jvm_error.log | JVM diagnostic flags |
| zk_server_jvm_min_memory | 64 | Min JVM heap memory(in MB, see -Xms) |
| zk_server_jvm_max_memory | 1024 | Max JVM heap memory(in MB, see -Xmx) |
| zk_server_jvm_memory_flags | -XX:MaxDirectMemorySize=128M -XX:ReservedCodeCacheSize=64m -XX:MetaspaceSize=16m -XX:MaxMetaspaceSize=48m -XX:NewRatio=3 -XX:+UseTLAB -XX:+UseLargePages | JVM memory allocation related flags |
| zk_client_jvm_max_memory | 256 | zk-Cli.sh JVM max heap memory(in MB) |
| zk_client_jvm_flags | empty by default  | zk-Cli.sh JVM custom parameters |

#### Zookeeper service settings

Next section contains configuration parameters which appears in zoo.cfg, zoo.cfg.dynamic files or passed on command line in format `-Dproperty=value`. Many of them have similar names with Zookeeper config properties and hence description omitted for them(see Zookeeper documentation for more info).

| Variable name | Default Value | Description|
| :--------: | :--------: | :--------: |
| zk_cfg_sys_digest_auth_provider_superdigest | empty by default | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.DigestAuthenticationProvider.superDigest` |
| zk_cfg_sys_fsync_threshold | 1000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.fsync.warningthresholdms` |
| zk_cfg_sys_readonly_mode | "false" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `readonlymode.enabled` |
| zk_cfg_sys_jute_maxbuffer | empty by default | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `jute.maxbuffer` |
| zk_cfg_sys_datadir_autocreate | "false" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.datadir.autocreate` |
| zk_cfg_sys_serverCnxnFactory | org.apache.zookeeper.server.NettyServerCnxnFactory | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for  environment variable `zookeeper.serverCnxnFactory` |
| zk_cfg_client_port | 2181 | The port to listen for client connections; that is, the port that clients attempt to connect to. This variable can be set per host(in inventory file or host_vars). |
| cfg_master_port | 2182 | The port that followers use to connect to the leaderThis variable can be set per host(in inventory file or host_vars). |
| cfg_master_port | 2183 | The port used for leader election.This variable can be set per host(in inventory file or host_vars). |
| cfg_client_bind_address | 0.0.0.0 | IP address used to accept client connections. `zk_cfg_client_port` will be opened on this IP. This variable can be set per host(in inventory file or host_vars). |
| cfg_quorum_bind_address | Not set by default | IP address to accept connections for quorum communication. This variable can be set per host(in inventory file or host_vars). |
| cfg_auto_bind_address | Empty by default(use inventory hostname) | Script can automatically infer bind addresses(`cfg_client_bind_address` and `cfg_quorum_bind_address`) for Zookeeper node based on value of this variable. *Note*: this variable will be ignored for node if you set `cfg_client_bind_address` and `cfg_quorum_bind_address` values explicitly. Possible values: **default_ipv4**(see `ansible_default_ipv4` Ansible host var), **ansible_nodename**(see `ansible_nodename` Ansible host var), **ansible_host**(see `ansible_host` Ansible host var), otherwise **inventory hostname**. |
| cfg_quorum_listen_on_all_ip | "false" | When set to `true` the ZooKeeper server will listen for connections from its peers on all available IP addresses, and not only the address configured in the server list of the configuration file. Default value is `false`. |
| cfg_server_role | participant | Zookeeper node role: `participant` or `observer` |
| cfg_tick_time | 2000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `tickTime` |
| cfg_global_outstanding_limit | 1000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `globalOutstandingLimit` |
| cfg_prealloc_size | 65535 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `preAllocSize` |
| cfg_snap_count | 100000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `snapCount` |
| cfg_max_client_connections | 0 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `maxClientCnxns` |
| cfg_min_session_timeout | 4000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `minSessionTimeout` |
| cfg_max_session_timeout | 20000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `maxSessionTimeout` |
| cfg_autopurge_snap_retain_count | 3 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `autopurge.snapRetainCount` |
| cfg_autopurge_interval | 2 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `autopurge.purgeInterval` |
| cfg_sync_enabled | "true" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `syncEnabled` |
| cfg_election_algorithm | 3 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `electionAlg` |
| cfg_leader_serves_clients | yes | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `leaderServes` |
| cfg_init_limit | 10 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `initLimit` |
| cfg_sync_limit | 10 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `syncLimit` |
| cfg_cnx_timeout | 5 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `cnxTimeout` |
| cfg_standalone | "false" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `standaloneEnabled` |
| cfg_force_sync | "yes" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `forceSync` |
| cfg_skip_acl | "false" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `skipACL` |
| cfg_nio_selector_threads | {{ ansible_processor_vcpus }} | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.nio.numSelectorThreads` |
| cfg_nio_worker_threads | {{ ansible_processor_vcpus * 2 }} | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.nio.numWorkerThreads` |
| cfg_commitprocessor_worker_threads | {{ ansible_processor_vcpus }} | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `zookeeper.commitProcessor.numWorkerThreads` |
| cfg_admin_server | "true" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `admin.enableServer` |
| cfg_admin_server_address | 0.0.0.0 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `admin.serverAddress` |
| cfg_admin_server_port | 8080 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `admin.serverPort` |
| cfg_admin_url | "/commands" | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `admin.commandURL` |
| zk_cfg_znode_container_check_interval_ms | 60000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `znode.container.checkIntervalMs` |
| zk_cfg_znode_container_max_per_minute | 10000 | See [ZK documentation](http://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html) for `znode.container.maxPerMinute` |


## Zookeeper binaries version upgrade
This section describes how to start upgrade process instead of usual deploy. This mode doesn't remove data/log dirs, but only update config files and Zookeeper binaries according version set in `zk_version` variable.

To start upgrade set variable `zk_upgrade` to `True`.  
You can freely change Zookeeper configuration(service user, ZK configuration properties, systemd service settings, JVM flags, JMX) during upgrade using previously mentioned variables(see section "*Configurable variables*").  
But take into account some exceptional cases:  
1. variables which set folders for Zookeeper service(binary, config, data/tran_logs, logs, etc) must have the **same** values as it was during initial deploy(script **doesn't** create folders during upgrade, only exception is `tmp_dir`).  
2. variable value changes related to dynamic configuration will not be affected by upgrade. This include node bind addresses and ports(client and quorum) and it role(observer or participant).

*Note*: Dynamic config files(which Zookeeper create when you add or delete nodes) will not be touched and hence `myid` files also remain unchanged.

**Warning**:  
1. do not upgrade production cluster without testing this script on some test environment. All software can contains bugs and can break your production:)  
2. Also be warned about case when you set `zk_cfg_data_dir` and `cfg_data_log_dir` to folder which is subfolder of `zk_bin_dir`: script unconditionally remove binary folder and all it content(and hence all your data). Same story with log folders.

*Optional*: to remove data/log folders you can set `zk_upgrade_remove_data_log_dir` to `True`. *Be warned*, that action remove all your data stored in Zookeeper.

## Reconfiguration without binary upgrade
It is very common way that you want to update Zookeeper configuration.  
For this purpose script tags modules related to reconfiguration.
You can simply change variables and call your playbook with *reconfigure* tag:  
`ansible-playbook myZkPlaybook.yml -t reconfigure`

To restart service after reconfiguration, set *start_service* tag along with *reconfigure* tag:  
`ansible-playbook myZkPlaybook.yml -t "reconfigure,start_service"`


*Important note*: 
Reconfiguration has similar requirements as binary upgrade: you can't change folders where initial Zookeeper deployment was made and can't change ZK dynamic configuration files. More details in "*Zookeeper binaries version upgrade*" section.

## Prometheus JMX exporter
TODO: //